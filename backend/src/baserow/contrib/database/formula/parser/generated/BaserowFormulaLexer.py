# Generated from BaserowFormulaLexer.g4 by ANTLR 4.8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2T")
        buf.write("\u0282\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write('\t\36\4\37\t\37\4 \t \4!\t!\4"\t"\4#\t#\4$\t$\4%\t%')
        buf.write("\4&\t&\4'\t'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.")
        buf.write("\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64")
        buf.write("\t\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:")
        buf.write("\4;\t;\4<\t<\4=\t=\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\t")
        buf.write("C\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I\tI\4J\tJ\4K\tK\4L\t")
        buf.write("L\4M\tM\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\4T\tT\4U\t")
        buf.write("U\4V\tV\4W\tW\4X\tX\4Y\tY\4Z\tZ\4[\t[\4\\\t\\\4]\t]\4")
        buf.write("^\t^\4_\t_\4`\t`\4a\ta\4b\tb\4c\tc\4d\td\4e\te\4f\tf\4")
        buf.write("g\tg\4h\th\4i\ti\4j\tj\4k\tk\4l\tl\4m\tm\4n\tn\4o\to\4")
        buf.write("p\tp\4q\tq\4r\tr\4s\ts\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5")
        buf.write("\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13\3\f")
        buf.write("\3\f\3\r\3\r\3\16\3\16\3\17\3\17\3\20\3\20\3\21\3\21\3")
        buf.write("\22\3\22\3\23\3\23\3\24\3\24\3\25\3\25\3\26\3\26\3\27")
        buf.write("\3\27\3\30\3\30\3\31\3\31\3\32\3\32\3\33\3\33\3\34\3\34")
        buf.write("\3\35\3\35\3\36\3\36\3\37\3\37\3\37\3\37\7\37\u0126\n")
        buf.write("\37\f\37\16\37\u0129\13\37\3\37\3\37\3 \3 \3 \3 \7 \u0131")
        buf.write("\n \f \16 \u0134\13 \3 \3 \3!\3!\3!\3!\3!\3!\7!\u013e")
        buf.write('\n!\f!\16!\u0141\13!\3!\3!\3"\3"\3"\3"\7"\u0149\n')
        buf.write('"\f"\16"\u014c\13"\3"\3"\3"\3#\3#\3#\3#\7#\u0155')
        buf.write("\n#\f#\16#\u0158\13#\3$\6$\u015b\n$\r$\16$\u015c\3%\3")
        buf.write("%\3%\3%\3%\3&\3&\3&\3&\3&\3&\3'\3'\3'\3'\3'\3'\3")
        buf.write("(\3(\3(\3(\3(\3(\3(\3(\3(\3(\3(\3(\3)\3)\3*\3*\3+\3+\3")
        buf.write("+\3,\3,\3-\3-\3-\3.\3.\3/\3/\3\60\3\60\3\61\3\61\3\62")
        buf.write("\3\62\3\63\3\63\3\63\7\63\u0195\n\63\f\63\16\63\u0198")
        buf.write("\13\63\3\63\3\63\3\64\3\64\3\64\3\65\5\65\u01a0\n\65\3")
        buf.write("\65\6\65\u01a3\n\65\r\65\16\65\u01a4\3\65\3\65\6\65\u01a9")
        buf.write("\n\65\r\65\16\65\u01aa\3\65\3\65\7\65\u01af\n\65\f\65")
        buf.write("\16\65\u01b2\13\65\3\65\6\65\u01b5\n\65\r\65\16\65\u01b6")
        buf.write("\5\65\u01b9\n\65\3\66\5\66\u01bc\n\66\3\66\6\66\u01bf")
        buf.write("\n\66\r\66\16\66\u01c0\3\66\3\66\6\66\u01c5\n\66\r\66")
        buf.write("\16\66\u01c6\5\66\u01c9\n\66\3\67\3\67\3\67\38\38\39\3")
        buf.write("9\3:\3:\3;\3;\7;\u01d6\n;\f;\16;\u01d9\13;\3<\3<\7<\u01dd")
        buf.write("\n<\f<\16<\u01e0\13<\3=\3=\3>\3>\3>\3?\3?\3?\3@\3@\3@")
        buf.write("\3A\3A\3A\3B\3B\3C\3C\3D\3D\3D\3E\3E\3E\3F\3F\3G\3G\3")
        buf.write("H\3H\3H\3I\3I\3J\3J\3J\3K\3K\3K\3L\3L\3M\3M\3M\3N\3N\3")
        buf.write("N\3O\3O\3O\3O\3P\3P\3P\3Q\3Q\3Q\3R\3R\3R\3R\3S\3S\3S\3")
        buf.write("S\3T\3T\3U\3U\3U\3V\3V\3V\3W\3W\3W\3X\3X\3X\3Y\3Y\3Y\3")
        buf.write("Y\3Z\3Z\3Z\3[\3[\3[\3[\3\\\3\\\3\\\3\\\3]\3]\3^\3^\3_")
        buf.write("\3_\3`\3`\3`\3a\3a\3a\3a\3b\3b\3b\3c\3c\3d\3d\3e\3e\3")
        buf.write("e\3f\3f\3f\3g\3g\3g\3h\3h\3h\3i\3i\3j\3j\3k\3k\3k\3l\3")
        buf.write("l\3l\3l\3l\3m\3m\3m\3m\3n\3n\3n\3n\3n\3o\3o\3o\3o\3p\3")
        buf.write("p\3p\3q\3q\3q\3r\3r\3s\3s\2\2t\3\2\5\2\7\2\t\2\13\2\r")
        buf.write("\2\17\2\21\2\23\2\25\2\27\2\31\2\33\2\35\2\37\2!\2#\2")
        buf.write("%\2'\2)\2+\2-\2/\2\61\2\63\2\65\2\67\29\2;\2=\2?\2A\2")
        buf.write("C\3E\4G\5I\6K\7M\bO\tQ\nS\13U\fW\rY\16[\17]\20_\21a\22")
        buf.write("c\23e\24g\25i\26k\27m\30o\31q\32s\33u\34w\35y\36{\37}")
        buf.write(" \177!\u0081\"\u0083#\u0085$\u0087%\u0089&\u008b'\u008d")
        buf.write("(\u008f)\u0091*\u0093+\u0095,\u0097-\u0099.\u009b/\u009d")
        buf.write("\60\u009f\61\u00a1\62\u00a3\63\u00a5\64\u00a7\65\u00a9")
        buf.write("\66\u00ab\67\u00ad8\u00af9\u00b1:\u00b3;\u00b5<\u00b7")
        buf.write("=\u00b9>\u00bb?\u00bd@\u00bfA\u00c1B\u00c3C\u00c5D\u00c7")
        buf.write("E\u00c9F\u00cbG\u00cdH\u00cfI\u00d1J\u00d3K\u00d5L\u00d7")
        buf.write("M\u00d9N\u00dbO\u00ddP\u00dfQ\u00e1R\u00e3S\u00e5T\3\2")
        buf.write("'\4\2CCcc\4\2DDdd\4\2EEee\4\2FFff\4\2GGgg\4\2HHhh\4\2")
        buf.write("IIii\4\2JJjj\4\2KKkk\4\2LLll\4\2MMmm\4\2NNnn\4\2OOoo\4")
        buf.write("\2PPpp\4\2QQqq\4\2RRrr\4\2SSss\4\2TTtt\4\2UUuu\4\2VVv")
        buf.write("v\4\2WWww\4\2XXxx\4\2YYyy\4\2ZZzz\4\2[[{{\4\2\\\\||\4")
        buf.write("\2\62;CH\3\2\62;\4\2$$^^\4\2))^^\4\2^^bb\4\2\f\f\17\17")
        buf.write('\5\2\13\f\17\17""\5\2C\\aac|\6\2\62;C\\aac|\6\2C\\a')
        buf.write("ac|\u00a3\1\7\2\62;C\\aac|\u00a3\1\2\u0278\2C\3\2\2\2")
        buf.write("\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2")
        buf.write("\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2")
        buf.write("\2\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3\2\2\2\2a\3")
        buf.write("\2\2\2\2c\3\2\2\2\2e\3\2\2\2\2g\3\2\2\2\2i\3\2\2\2\2k")
        buf.write("\3\2\2\2\2m\3\2\2\2\2o\3\2\2\2\2q\3\2\2\2\2s\3\2\2\2\2")
        buf.write("u\3\2\2\2\2w\3\2\2\2\2y\3\2\2\2\2{\3\2\2\2\2}\3\2\2\2")
        buf.write("\2\177\3\2\2\2\2\u0081\3\2\2\2\2\u0083\3\2\2\2\2\u0085")
        buf.write("\3\2\2\2\2\u0087\3\2\2\2\2\u0089\3\2\2\2\2\u008b\3\2\2")
        buf.write("\2\2\u008d\3\2\2\2\2\u008f\3\2\2\2\2\u0091\3\2\2\2\2\u0093")
        buf.write("\3\2\2\2\2\u0095\3\2\2\2\2\u0097\3\2\2\2\2\u0099\3\2\2")
        buf.write("\2\2\u009b\3\2\2\2\2\u009d\3\2\2\2\2\u009f\3\2\2\2\2\u00a1")
        buf.write("\3\2\2\2\2\u00a3\3\2\2\2\2\u00a5\3\2\2\2\2\u00a7\3\2\2")
        buf.write("\2\2\u00a9\3\2\2\2\2\u00ab\3\2\2\2\2\u00ad\3\2\2\2\2\u00af")
        buf.write("\3\2\2\2\2\u00b1\3\2\2\2\2\u00b3\3\2\2\2\2\u00b5\3\2\2")
        buf.write("\2\2\u00b7\3\2\2\2\2\u00b9\3\2\2\2\2\u00bb\3\2\2\2\2\u00bd")
        buf.write("\3\2\2\2\2\u00bf\3\2\2\2\2\u00c1\3\2\2\2\2\u00c3\3\2\2")
        buf.write("\2\2\u00c5\3\2\2\2\2\u00c7\3\2\2\2\2\u00c9\3\2\2\2\2\u00cb")
        buf.write("\3\2\2\2\2\u00cd\3\2\2\2\2\u00cf\3\2\2\2\2\u00d1\3\2\2")
        buf.write("\2\2\u00d3\3\2\2\2\2\u00d5\3\2\2\2\2\u00d7\3\2\2\2\2\u00d9")
        buf.write("\3\2\2\2\2\u00db\3\2\2\2\2\u00dd\3\2\2\2\2\u00df\3\2\2")
        buf.write("\2\2\u00e1\3\2\2\2\2\u00e3\3\2\2\2\2\u00e5\3\2\2\2\3\u00e7")
        buf.write("\3\2\2\2\5\u00e9\3\2\2\2\7\u00eb\3\2\2\2\t\u00ed\3\2\2")
        buf.write("\2\13\u00ef\3\2\2\2\r\u00f1\3\2\2\2\17\u00f3\3\2\2\2\21")
        buf.write("\u00f5\3\2\2\2\23\u00f7\3\2\2\2\25\u00f9\3\2\2\2\27\u00fb")
        buf.write("\3\2\2\2\31\u00fd\3\2\2\2\33\u00ff\3\2\2\2\35\u0101\3")
        buf.write("\2\2\2\37\u0103\3\2\2\2!\u0105\3\2\2\2#\u0107\3\2\2\2")
        buf.write("%\u0109\3\2\2\2'\u010b\3\2\2\2)\u010d\3\2\2\2+\u010f")
        buf.write("\3\2\2\2-\u0111\3\2\2\2/\u0113\3\2\2\2\61\u0115\3\2\2")
        buf.write("\2\63\u0117\3\2\2\2\65\u0119\3\2\2\2\67\u011b\3\2\2\2")
        buf.write("9\u011d\3\2\2\2;\u011f\3\2\2\2=\u0121\3\2\2\2?\u012c\3")
        buf.write("\2\2\2A\u0137\3\2\2\2C\u0144\3\2\2\2E\u0150\3\2\2\2G\u015a")
        buf.write("\3\2\2\2I\u015e\3\2\2\2K\u0163\3\2\2\2M\u0169\3\2\2\2")
        buf.write("O\u016f\3\2\2\2Q\u017b\3\2\2\2S\u017d\3\2\2\2U\u017f\3")
        buf.write("\2\2\2W\u0182\3\2\2\2Y\u0184\3\2\2\2[\u0187\3\2\2\2]\u0189")
        buf.write("\3\2\2\2_\u018b\3\2\2\2a\u018d\3\2\2\2c\u018f\3\2\2\2")
        buf.write("e\u0191\3\2\2\2g\u019b\3\2\2\2i\u019f\3\2\2\2k\u01bb\3")
        buf.write("\2\2\2m\u01ca\3\2\2\2o\u01cd\3\2\2\2q\u01cf\3\2\2\2s\u01d1")
        buf.write("\3\2\2\2u\u01d3\3\2\2\2w\u01da\3\2\2\2y\u01e1\3\2\2\2")
        buf.write("{\u01e3\3\2\2\2}\u01e6\3\2\2\2\177\u01e9\3\2\2\2\u0081")
        buf.write("\u01ec\3\2\2\2\u0083\u01ef\3\2\2\2\u0085\u01f1\3\2\2\2")
        buf.write("\u0087\u01f3\3\2\2\2\u0089\u01f6\3\2\2\2\u008b\u01f9\3")
        buf.write("\2\2\2\u008d\u01fb\3\2\2\2\u008f\u01fd\3\2\2\2\u0091\u0200")
        buf.write("\3\2\2\2\u0093\u0202\3\2\2\2\u0095\u0205\3\2\2\2\u0097")
        buf.write("\u0208\3\2\2\2\u0099\u020a\3\2\2\2\u009b\u020d\3\2\2\2")
        buf.write("\u009d\u0210\3\2\2\2\u009f\u0214\3\2\2\2\u00a1\u0217\3")
        buf.write("\2\2\2\u00a3\u021a\3\2\2\2\u00a5\u021e\3\2\2\2\u00a7\u0222")
        buf.write("\3\2\2\2\u00a9\u0224\3\2\2\2\u00ab\u0227\3\2\2\2\u00ad")
        buf.write("\u022a\3\2\2\2\u00af\u022d\3\2\2\2\u00b1\u0230\3\2\2\2")
        buf.write("\u00b3\u0234\3\2\2\2\u00b5\u0237\3\2\2\2\u00b7\u023b\3")
        buf.write("\2\2\2\u00b9\u023f\3\2\2\2\u00bb\u0241\3\2\2\2\u00bd\u0243")
        buf.write("\3\2\2\2\u00bf\u0245\3\2\2\2\u00c1\u0248\3\2\2\2\u00c3")
        buf.write("\u024c\3\2\2\2\u00c5\u024f\3\2\2\2\u00c7\u0251\3\2\2\2")
        buf.write("\u00c9\u0253\3\2\2\2\u00cb\u0256\3\2\2\2\u00cd\u0259\3")
        buf.write("\2\2\2\u00cf\u025c\3\2\2\2\u00d1\u025f\3\2\2\2\u00d3\u0261")
        buf.write("\3\2\2\2\u00d5\u0263\3\2\2\2\u00d7\u0266\3\2\2\2\u00d9")
        buf.write("\u026b\3\2\2\2\u00db\u026f\3\2\2\2\u00dd\u0274\3\2\2\2")
        buf.write("\u00df\u0278\3\2\2\2\u00e1\u027b\3\2\2\2\u00e3\u027e\3")
        buf.write("\2\2\2\u00e5\u0280\3\2\2\2\u00e7\u00e8\t\2\2\2\u00e8\4")
        buf.write("\3\2\2\2\u00e9\u00ea\t\3\2\2\u00ea\6\3\2\2\2\u00eb\u00ec")
        buf.write("\t\4\2\2\u00ec\b\3\2\2\2\u00ed\u00ee\t\5\2\2\u00ee\n\3")
        buf.write("\2\2\2\u00ef\u00f0\t\6\2\2\u00f0\f\3\2\2\2\u00f1\u00f2")
        buf.write("\t\7\2\2\u00f2\16\3\2\2\2\u00f3\u00f4\t\b\2\2\u00f4\20")
        buf.write("\3\2\2\2\u00f5\u00f6\t\t\2\2\u00f6\22\3\2\2\2\u00f7\u00f8")
        buf.write("\t\n\2\2\u00f8\24\3\2\2\2\u00f9\u00fa\t\13\2\2\u00fa\26")
        buf.write("\3\2\2\2\u00fb\u00fc\t\f\2\2\u00fc\30\3\2\2\2\u00fd\u00fe")
        buf.write("\t\r\2\2\u00fe\32\3\2\2\2\u00ff\u0100\t\16\2\2\u0100\34")
        buf.write("\3\2\2\2\u0101\u0102\t\17\2\2\u0102\36\3\2\2\2\u0103\u0104")
        buf.write('\t\20\2\2\u0104 \3\2\2\2\u0105\u0106\t\21\2\2\u0106"')
        buf.write("\3\2\2\2\u0107\u0108\t\22\2\2\u0108$\3\2\2\2\u0109\u010a")
        buf.write("\t\23\2\2\u010a&\3\2\2\2\u010b\u010c\t\24\2\2\u010c(\3")
        buf.write("\2\2\2\u010d\u010e\t\25\2\2\u010e*\3\2\2\2\u010f\u0110")
        buf.write("\t\26\2\2\u0110,\3\2\2\2\u0111\u0112\t\27\2\2\u0112.\3")
        buf.write("\2\2\2\u0113\u0114\t\30\2\2\u0114\60\3\2\2\2\u0115\u0116")
        buf.write("\t\31\2\2\u0116\62\3\2\2\2\u0117\u0118\t\32\2\2\u0118")
        buf.write("\64\3\2\2\2\u0119\u011a\t\33\2\2\u011a\66\3\2\2\2\u011b")
        buf.write("\u011c\7a\2\2\u011c8\3\2\2\2\u011d\u011e\t\34\2\2\u011e")
        buf.write(":\3\2\2\2\u011f\u0120\t\35\2\2\u0120<\3\2\2\2\u0121\u0127")
        buf.write("\7$\2\2\u0122\u0123\7^\2\2\u0123\u0126\13\2\2\2\u0124")
        buf.write("\u0126\n\36\2\2\u0125\u0122\3\2\2\2\u0125\u0124\3\2\2")
        buf.write("\2\u0126\u0129\3\2\2\2\u0127\u0125\3\2\2\2\u0127\u0128")
        buf.write("\3\2\2\2\u0128\u012a\3\2\2\2\u0129\u0127\3\2\2\2\u012a")
        buf.write("\u012b\7$\2\2\u012b>\3\2\2\2\u012c\u0132\7)\2\2\u012d")
        buf.write("\u012e\7^\2\2\u012e\u0131\13\2\2\2\u012f\u0131\n\37\2")
        buf.write("\2\u0130\u012d\3\2\2\2\u0130\u012f\3\2\2\2\u0131\u0134")
        buf.write("\3\2\2\2\u0132\u0130\3\2\2\2\u0132\u0133\3\2\2\2\u0133")
        buf.write("\u0135\3\2\2\2\u0134\u0132\3\2\2\2\u0135\u0136\7)\2\2")
        buf.write("\u0136@\3\2\2\2\u0137\u013f\7b\2\2\u0138\u0139\7^\2\2")
        buf.write("\u0139\u013e\13\2\2\2\u013a\u013b\7b\2\2\u013b\u013e\7")
        buf.write("b\2\2\u013c\u013e\n \2\2\u013d\u0138\3\2\2\2\u013d\u013a")
        buf.write("\3\2\2\2\u013d\u013c\3\2\2\2\u013e\u0141\3\2\2\2\u013f")
        buf.write("\u013d\3\2\2\2\u013f\u0140\3\2\2\2\u0140\u0142\3\2\2\2")
        buf.write("\u0141\u013f\3\2\2\2\u0142\u0143\7b\2\2\u0143B\3\2\2\2")
        buf.write("\u0144\u0145\7\61\2\2\u0145\u0146\7,\2\2\u0146\u014a\3")
        buf.write("\2\2\2\u0147\u0149\13\2\2\2\u0148\u0147\3\2\2\2\u0149")
        buf.write("\u014c\3\2\2\2\u014a\u0148\3\2\2\2\u014a\u014b\3\2\2\2")
        buf.write("\u014b\u014d\3\2\2\2\u014c\u014a\3\2\2\2\u014d\u014e\7")
        buf.write(",\2\2\u014e\u014f\7\61\2\2\u014fD\3\2\2\2\u0150\u0151")
        buf.write("\7\61\2\2\u0151\u0152\7\61\2\2\u0152\u0156\3\2\2\2\u0153")
        buf.write("\u0155\n!\2\2\u0154\u0153\3\2\2\2\u0155\u0158\3\2\2\2")
        buf.write("\u0156\u0154\3\2\2\2\u0156\u0157\3\2\2\2\u0157F\3\2\2")
        buf.write('\2\u0158\u0156\3\2\2\2\u0159\u015b\t"\2\2\u015a\u0159')
        buf.write("\3\2\2\2\u015b\u015c\3\2\2\2\u015c\u015a\3\2\2\2\u015c")
        buf.write("\u015d\3\2\2\2\u015dH\3\2\2\2\u015e\u015f\5)\25\2\u015f")
        buf.write("\u0160\5%\23\2\u0160\u0161\5+\26\2\u0161\u0162\5\13\6")
        buf.write("\2\u0162J\3\2\2\2\u0163\u0164\5\r\7\2\u0164\u0165\5\3")
        buf.write("\2\2\u0165\u0166\5\31\r\2\u0166\u0167\5'\24\2\u0167\u0168")
        buf.write("\5\13\6\2\u0168L\3\2\2\2\u0169\u016a\5\r\7\2\u016a\u016b")
        buf.write("\5\23\n\2\u016b\u016c\5\13\6\2\u016c\u016d\5\31\r\2\u016d")
        buf.write("\u016e\5\t\5\2\u016eN\3\2\2\2\u016f\u0170\5\r\7\2\u0170")
        buf.write("\u0171\5\23\n\2\u0171\u0172\5\13\6\2\u0172\u0173\5\31")
        buf.write("\r\2\u0173\u0174\5\t\5\2\u0174\u0175\5\67\34\2\u0175\u0176")
        buf.write("\5\5\3\2\u0176\u0177\5\63\32\2\u0177\u0178\5\67\34\2\u0178")
        buf.write("\u0179\5\23\n\2\u0179\u017a\5\t\5\2\u017aP\3\2\2\2\u017b")
        buf.write("\u017c\7.\2\2\u017cR\3\2\2\2\u017d\u017e\7<\2\2\u017e")
        buf.write("T\3\2\2\2\u017f\u0180\7<\2\2\u0180\u0181\7<\2\2\u0181")
        buf.write("V\3\2\2\2\u0182\u0183\7&\2\2\u0183X\3\2\2\2\u0184\u0185")
        buf.write("\7&\2\2\u0185\u0186\7&\2\2\u0186Z\3\2\2\2\u0187\u0188")
        buf.write("\7,\2\2\u0188\\\3\2\2\2\u0189\u018a\7*\2\2\u018a^\3\2")
        buf.write("\2\2\u018b\u018c\7+\2\2\u018c`\3\2\2\2\u018d\u018e\7]")
        buf.write("\2\2\u018eb\3\2\2\2\u018f\u0190\7_\2\2\u0190d\3\2\2\2")
        buf.write("\u0191\u0192\5\5\3\2\u0192\u0196\7)\2\2\u0193\u0195\4")
        buf.write("\62\63\2\u0194\u0193\3\2\2\2\u0195\u0198\3\2\2\2\u0196")
        buf.write("\u0194\3\2\2\2\u0196\u0197\3\2\2\2\u0197\u0199\3\2\2\2")
        buf.write("\u0198\u0196\3\2\2\2\u0199\u019a\7)\2\2\u019af\3\2\2\2")
        buf.write("\u019b\u019c\5\13\6\2\u019c\u019d\5? \2\u019dh\3\2\2\2")
        buf.write("\u019e\u01a0\7/\2\2\u019f\u019e\3\2\2\2\u019f\u01a0\3")
        buf.write("\2\2\2\u01a0\u01a2\3\2\2\2\u01a1\u01a3\5;\36\2\u01a2\u01a1")
        buf.write("\3\2\2\2\u01a3\u01a4\3\2\2\2\u01a4\u01a2\3\2\2\2\u01a4")
        buf.write("\u01a5\3\2\2\2\u01a5\u01a6\3\2\2\2\u01a6\u01a8\7\60\2")
        buf.write("\2\u01a7\u01a9\5;\36\2\u01a8\u01a7\3\2\2\2\u01a9\u01aa")
        buf.write("\3\2\2\2\u01aa\u01a8\3\2\2\2\u01aa\u01ab\3\2\2\2\u01ab")
        buf.write("\u01b8\3\2\2\2\u01ac\u01b0\5\13\6\2\u01ad\u01af\7/\2\2")
        buf.write("\u01ae\u01ad\3\2\2\2\u01af\u01b2\3\2\2\2\u01b0\u01ae\3")
        buf.write("\2\2\2\u01b0\u01b1\3\2\2\2\u01b1\u01b4\3\2\2\2\u01b2\u01b0")
        buf.write("\3\2\2\2\u01b3\u01b5\5;\36\2\u01b4\u01b3\3\2\2\2\u01b5")
        buf.write("\u01b6\3\2\2\2\u01b6\u01b4\3\2\2\2\u01b6\u01b7\3\2\2\2")
        buf.write("\u01b7\u01b9\3\2\2\2\u01b8\u01ac\3\2\2\2\u01b8\u01b9\3")
        buf.write("\2\2\2\u01b9j\3\2\2\2\u01ba\u01bc\7/\2\2\u01bb\u01ba\3")
        buf.write("\2\2\2\u01bb\u01bc\3\2\2\2\u01bc\u01be\3\2\2\2\u01bd\u01bf")
        buf.write("\5;\36\2\u01be\u01bd\3\2\2\2\u01bf\u01c0\3\2\2\2\u01c0")
        buf.write("\u01be\3\2\2\2\u01c0\u01c1\3\2\2\2\u01c1\u01c8\3\2\2\2")
        buf.write("\u01c2\u01c4\5\13\6\2\u01c3\u01c5\5;\36\2\u01c4\u01c3")
        buf.write("\3\2\2\2\u01c5\u01c6\3\2\2\2\u01c6\u01c4\3\2\2\2\u01c6")
        buf.write("\u01c7\3\2\2\2\u01c7\u01c9\3\2\2\2\u01c8\u01c2\3\2\2\2")
        buf.write("\u01c8\u01c9\3\2\2\2\u01c9l\3\2\2\2\u01ca\u01cb\7z\2\2")
        buf.write("\u01cb\u01cc\5? \2\u01ccn\3\2\2\2\u01cd\u01ce\7\60\2\2")
        buf.write("\u01cep\3\2\2\2\u01cf\u01d0\5? \2\u01d0r\3\2\2\2\u01d1")
        buf.write("\u01d2\5=\37\2\u01d2t\3\2\2\2\u01d3\u01d7\t#\2\2\u01d4")
        buf.write("\u01d6\t$\2\2\u01d5\u01d4\3\2\2\2\u01d6\u01d9\3\2\2\2")
        buf.write("\u01d7\u01d5\3\2\2\2\u01d7\u01d8\3\2\2\2\u01d8v\3\2\2")
        buf.write("\2\u01d9\u01d7\3\2\2\2\u01da\u01de\t%\2\2\u01db\u01dd")
        buf.write("\t&\2\2\u01dc\u01db\3\2\2\2\u01dd\u01e0\3\2\2\2\u01de")
        buf.write("\u01dc\3\2\2\2\u01de\u01df\3\2\2\2\u01dfx\3\2\2\2\u01e0")
        buf.write("\u01de\3\2\2\2\u01e1\u01e2\7(\2\2\u01e2z\3\2\2\2\u01e3")
        buf.write("\u01e4\7(\2\2\u01e4\u01e5\7(\2\2\u01e5|\3\2\2\2\u01e6")
        buf.write("\u01e7\7(\2\2\u01e7\u01e8\7>\2\2\u01e8~\3\2\2\2\u01e9")
        buf.write("\u01ea\7B\2\2\u01ea\u01eb\7B\2\2\u01eb\u0080\3\2\2\2\u01ec")
        buf.write("\u01ed\7B\2\2\u01ed\u01ee\7@\2\2\u01ee\u0082\3\2\2\2\u01ef")
        buf.write("\u01f0\7B\2\2\u01f0\u0084\3\2\2\2\u01f1\u01f2\7#\2\2\u01f2")
        buf.write("\u0086\3\2\2\2\u01f3\u01f4\7#\2\2\u01f4\u01f5\7#\2\2\u01f5")
        buf.write("\u0088\3\2\2\2\u01f6\u01f7\7#\2\2\u01f7\u01f8\7?\2\2\u01f8")
        buf.write("\u008a\3\2\2\2\u01f9\u01fa\7`\2\2\u01fa\u008c\3\2\2\2")
        buf.write("\u01fb\u01fc\7?\2\2\u01fc\u008e\3\2\2\2\u01fd\u01fe\7")
        buf.write("?\2\2\u01fe\u01ff\7@\2\2\u01ff\u0090\3\2\2\2\u0200\u0201")
        buf.write("\7@\2\2\u0201\u0092\3\2\2\2\u0202\u0203\7@\2\2\u0203\u0204")
        buf.write("\7?\2\2\u0204\u0094\3\2\2\2\u0205\u0206\7@\2\2\u0206\u0207")
        buf.write("\7@\2\2\u0207\u0096\3\2\2\2\u0208\u0209\7%\2\2\u0209\u0098")
        buf.write("\3\2\2\2\u020a\u020b\7%\2\2\u020b\u020c\7?\2\2\u020c\u009a")
        buf.write("\3\2\2\2\u020d\u020e\7%\2\2\u020e\u020f\7@\2\2\u020f\u009c")
        buf.write("\3\2\2\2\u0210\u0211\7%\2\2\u0211\u0212\7@\2\2\u0212\u0213")
        buf.write("\7@\2\2\u0213\u009e\3\2\2\2\u0214\u0215\7%\2\2\u0215\u0216")
        buf.write("\7%\2\2\u0216\u00a0\3\2\2\2\u0217\u0218\7/\2\2\u0218\u0219")
        buf.write("\7@\2\2\u0219\u00a2\3\2\2\2\u021a\u021b\7/\2\2\u021b\u021c")
        buf.write("\7@\2\2\u021c\u021d\7@\2\2\u021d\u00a4\3\2\2\2\u021e\u021f")
        buf.write("\7/\2\2\u021f\u0220\7~\2\2\u0220\u0221\7/\2\2\u0221\u00a6")
        buf.write("\3\2\2\2\u0222\u0223\7>\2\2\u0223\u00a8\3\2\2\2\u0224")
        buf.write("\u0225\7>\2\2\u0225\u0226\7?\2\2\u0226\u00aa\3\2\2\2\u0227")
        buf.write("\u0228\7>\2\2\u0228\u0229\7B\2\2\u0229\u00ac\3\2\2\2\u022a")
        buf.write("\u022b\7>\2\2\u022b\u022c\7`\2\2\u022c\u00ae\3\2\2\2\u022d")
        buf.write("\u022e\7>\2\2\u022e\u022f\7@\2\2\u022f\u00b0\3\2\2\2\u0230")
        buf.write("\u0231\7>\2\2\u0231\u0232\7/\2\2\u0232\u0233\7@\2\2\u0233")
        buf.write("\u00b2\3\2\2\2\u0234\u0235\7>\2\2\u0235\u0236\7>\2\2\u0236")
        buf.write("\u00b4\3\2\2\2\u0237\u0238\7>\2\2\u0238\u0239\7>\2\2\u0239")
        buf.write("\u023a\7?\2\2\u023a\u00b6\3\2\2\2\u023b\u023c\7>\2\2\u023c")
        buf.write("\u023d\7A\2\2\u023d\u023e\7@\2\2\u023e\u00b8\3\2\2\2\u023f")
        buf.write("\u0240\7/\2\2\u0240\u00ba\3\2\2\2\u0241\u0242\7'\2\2")
        buf.write("\u0242\u00bc\3\2\2\2\u0243\u0244\7~\2\2\u0244\u00be\3")
        buf.write("\2\2\2\u0245\u0246\7~\2\2\u0246\u0247\7~\2\2\u0247\u00c0")
        buf.write("\3\2\2\2\u0248\u0249\7~\2\2\u0249\u024a\7~\2\2\u024a\u024b")
        buf.write("\7\61\2\2\u024b\u00c2\3\2\2\2\u024c\u024d\7~\2\2\u024d")
        buf.write("\u024e\7\61\2\2\u024e\u00c4\3\2\2\2\u024f\u0250\7-\2\2")
        buf.write("\u0250\u00c6\3\2\2\2\u0251\u0252\7A\2\2\u0252\u00c8\3")
        buf.write("\2\2\2\u0253\u0254\7A\2\2\u0254\u0255\7(\2\2\u0255\u00ca")
        buf.write("\3\2\2\2\u0256\u0257\7A\2\2\u0257\u0258\7%\2\2\u0258\u00cc")
        buf.write("\3\2\2\2\u0259\u025a\7A\2\2\u025a\u025b\7/\2\2\u025b\u00ce")
        buf.write("\3\2\2\2\u025c\u025d\7A\2\2\u025d\u025e\7~\2\2\u025e\u00d0")
        buf.write("\3\2\2\2\u025f\u0260\7\61\2\2\u0260\u00d2\3\2\2\2\u0261")
        buf.write("\u0262\7\u0080\2\2\u0262\u00d4\3\2\2\2\u0263\u0264\7\u0080")
        buf.write("\2\2\u0264\u0265\7?\2\2\u0265\u00d6\3\2\2\2\u0266\u0267")
        buf.write("\7\u0080\2\2\u0267\u0268\7@\2\2\u0268\u0269\7?\2\2\u0269")
        buf.write("\u026a\7\u0080\2\2\u026a\u00d8\3\2\2\2\u026b\u026c\7\u0080")
        buf.write("\2\2\u026c\u026d\7@\2\2\u026d\u026e\7\u0080\2\2\u026e")
        buf.write("\u00da\3\2\2\2\u026f\u0270\7\u0080\2\2\u0270\u0271\7>")
        buf.write("\2\2\u0271\u0272\7?\2\2\u0272\u0273\7\u0080\2\2\u0273")
        buf.write("\u00dc\3\2\2\2\u0274\u0275\7\u0080\2\2\u0275\u0276\7>")
        buf.write("\2\2\u0276\u0277\7\u0080\2\2\u0277\u00de\3\2\2\2\u0278")
        buf.write("\u0279\7\u0080\2\2\u0279\u027a\7,\2\2\u027a\u00e0\3\2")
        buf.write("\2\2\u027b\u027c\7\u0080\2\2\u027c\u027d\7\u0080\2\2\u027d")
        buf.write("\u00e2\3\2\2\2\u027e\u027f\7=\2\2\u027f\u00e4\3\2\2\2")
        buf.write("\u0280\u0281\13\2\2\2\u0281\u00e6\3\2\2\2\31\2\u0125\u0127")
        buf.write("\u0130\u0132\u013d\u013f\u014a\u0156\u015c\u0196\u019f")
        buf.write("\u01a4\u01aa\u01b0\u01b6\u01b8\u01bb\u01c0\u01c6\u01c8")
        buf.write("\u01d7\u01de\2")
        return buf.getvalue()


class BaserowFormulaLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [DFA(ds, i) for i, ds in enumerate(atn.decisionToState)]

    BLOCK_COMMENT = 1
    LINE_COMMENT = 2
    WHITESPACE = 3
    TRUE = 4
    FALSE = 5
    FIELD = 6
    FIELDBYID = 7
    COMMA = 8
    COLON = 9
    COLON_COLON = 10
    DOLLAR = 11
    DOLLAR_DOLLAR = 12
    STAR = 13
    OPEN_PAREN = 14
    CLOSE_PAREN = 15
    OPEN_BRACKET = 16
    CLOSE_BRACKET = 17
    BIT_STRING = 18
    REGEX_STRING = 19
    NUMERIC_LITERAL = 20
    INTEGER_LITERAL = 21
    HEX_INTEGER_LITERAL = 22
    DOT = 23
    SINGLEQ_STRING_LITERAL = 24
    DOUBLEQ_STRING_LITERAL = 25
    IDENTIFIER = 26
    IDENTIFIER_UNICODE = 27
    AMP = 28
    AMP_AMP = 29
    AMP_LT = 30
    AT_AT = 31
    AT_GT = 32
    AT_SIGN = 33
    BANG = 34
    BANG_BANG = 35
    BANG_EQUAL = 36
    CARET = 37
    EQUAL = 38
    EQUAL_GT = 39
    GT = 40
    GTE = 41
    GT_GT = 42
    HASH = 43
    HASH_EQ = 44
    HASH_GT = 45
    HASH_GT_GT = 46
    HASH_HASH = 47
    HYPHEN_GT = 48
    HYPHEN_GT_GT = 49
    HYPHEN_PIPE_HYPHEN = 50
    LT = 51
    LTE = 52
    LT_AT = 53
    LT_CARET = 54
    LT_GT = 55
    LT_HYPHEN_GT = 56
    LT_LT = 57
    LT_LT_EQ = 58
    LT_QMARK_GT = 59
    MINUS = 60
    PERCENT = 61
    PIPE = 62
    PIPE_PIPE = 63
    PIPE_PIPE_SLASH = 64
    PIPE_SLASH = 65
    PLUS = 66
    QMARK = 67
    QMARK_AMP = 68
    QMARK_HASH = 69
    QMARK_HYPHEN = 70
    QMARK_PIPE = 71
    SLASH = 72
    TIL = 73
    TIL_EQ = 74
    TIL_GTE_TIL = 75
    TIL_GT_TIL = 76
    TIL_LTE_TIL = 77
    TIL_LT_TIL = 78
    TIL_STAR = 79
    TIL_TIL = 80
    SEMI = 81
    ErrorCharacter = 82

    channelNames = [u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN"]

    modeNames = ["DEFAULT_MODE"]

    literalNames = [
        "<INVALID>",
        "','",
        "':'",
        "'::'",
        "'$'",
        "'$$'",
        "'*'",
        "'('",
        "')'",
        "'['",
        "']'",
        "'.'",
        "'&'",
        "'&&'",
        "'&<'",
        "'@@'",
        "'@>'",
        "'@'",
        "'!'",
        "'!!'",
        "'!='",
        "'^'",
        "'='",
        "'=>'",
        "'>'",
        "'>='",
        "'>>'",
        "'#'",
        "'#='",
        "'#>'",
        "'#>>'",
        "'##'",
        "'->'",
        "'->>'",
        "'-|-'",
        "'<'",
        "'<='",
        "'<@'",
        "'<^'",
        "'<>'",
        "'<->'",
        "'<<'",
        "'<<='",
        "'<?>'",
        "'-'",
        "'%'",
        "'|'",
        "'||'",
        "'||/'",
        "'|/'",
        "'+'",
        "'?'",
        "'?&'",
        "'?#'",
        "'?-'",
        "'?|'",
        "'/'",
        "'~'",
        "'~='",
        "'~>=~'",
        "'~>~'",
        "'~<=~'",
        "'~<~'",
        "'~*'",
        "'~~'",
        "';'",
    ]

    symbolicNames = [
        "<INVALID>",
        "BLOCK_COMMENT",
        "LINE_COMMENT",
        "WHITESPACE",
        "TRUE",
        "FALSE",
        "FIELD",
        "FIELDBYID",
        "COMMA",
        "COLON",
        "COLON_COLON",
        "DOLLAR",
        "DOLLAR_DOLLAR",
        "STAR",
        "OPEN_PAREN",
        "CLOSE_PAREN",
        "OPEN_BRACKET",
        "CLOSE_BRACKET",
        "BIT_STRING",
        "REGEX_STRING",
        "NUMERIC_LITERAL",
        "INTEGER_LITERAL",
        "HEX_INTEGER_LITERAL",
        "DOT",
        "SINGLEQ_STRING_LITERAL",
        "DOUBLEQ_STRING_LITERAL",
        "IDENTIFIER",
        "IDENTIFIER_UNICODE",
        "AMP",
        "AMP_AMP",
        "AMP_LT",
        "AT_AT",
        "AT_GT",
        "AT_SIGN",
        "BANG",
        "BANG_BANG",
        "BANG_EQUAL",
        "CARET",
        "EQUAL",
        "EQUAL_GT",
        "GT",
        "GTE",
        "GT_GT",
        "HASH",
        "HASH_EQ",
        "HASH_GT",
        "HASH_GT_GT",
        "HASH_HASH",
        "HYPHEN_GT",
        "HYPHEN_GT_GT",
        "HYPHEN_PIPE_HYPHEN",
        "LT",
        "LTE",
        "LT_AT",
        "LT_CARET",
        "LT_GT",
        "LT_HYPHEN_GT",
        "LT_LT",
        "LT_LT_EQ",
        "LT_QMARK_GT",
        "MINUS",
        "PERCENT",
        "PIPE",
        "PIPE_PIPE",
        "PIPE_PIPE_SLASH",
        "PIPE_SLASH",
        "PLUS",
        "QMARK",
        "QMARK_AMP",
        "QMARK_HASH",
        "QMARK_HYPHEN",
        "QMARK_PIPE",
        "SLASH",
        "TIL",
        "TIL_EQ",
        "TIL_GTE_TIL",
        "TIL_GT_TIL",
        "TIL_LTE_TIL",
        "TIL_LT_TIL",
        "TIL_STAR",
        "TIL_TIL",
        "SEMI",
        "ErrorCharacter",
    ]

    ruleNames = [
        "A",
        "B",
        "C",
        "D",
        "E",
        "F",
        "G",
        "H",
        "I",
        "J",
        "K",
        "L",
        "M",
        "N",
        "O",
        "P",
        "Q",
        "R",
        "S",
        "T",
        "U",
        "V",
        "W",
        "X",
        "Y",
        "Z",
        "UNDERSCORE",
        "HEX_DIGIT",
        "DEC_DIGIT",
        "DQUOTA_STRING",
        "SQUOTA_STRING",
        "BQUOTA_STRING",
        "BLOCK_COMMENT",
        "LINE_COMMENT",
        "WHITESPACE",
        "TRUE",
        "FALSE",
        "FIELD",
        "FIELDBYID",
        "COMMA",
        "COLON",
        "COLON_COLON",
        "DOLLAR",
        "DOLLAR_DOLLAR",
        "STAR",
        "OPEN_PAREN",
        "CLOSE_PAREN",
        "OPEN_BRACKET",
        "CLOSE_BRACKET",
        "BIT_STRING",
        "REGEX_STRING",
        "NUMERIC_LITERAL",
        "INTEGER_LITERAL",
        "HEX_INTEGER_LITERAL",
        "DOT",
        "SINGLEQ_STRING_LITERAL",
        "DOUBLEQ_STRING_LITERAL",
        "IDENTIFIER",
        "IDENTIFIER_UNICODE",
        "AMP",
        "AMP_AMP",
        "AMP_LT",
        "AT_AT",
        "AT_GT",
        "AT_SIGN",
        "BANG",
        "BANG_BANG",
        "BANG_EQUAL",
        "CARET",
        "EQUAL",
        "EQUAL_GT",
        "GT",
        "GTE",
        "GT_GT",
        "HASH",
        "HASH_EQ",
        "HASH_GT",
        "HASH_GT_GT",
        "HASH_HASH",
        "HYPHEN_GT",
        "HYPHEN_GT_GT",
        "HYPHEN_PIPE_HYPHEN",
        "LT",
        "LTE",
        "LT_AT",
        "LT_CARET",
        "LT_GT",
        "LT_HYPHEN_GT",
        "LT_LT",
        "LT_LT_EQ",
        "LT_QMARK_GT",
        "MINUS",
        "PERCENT",
        "PIPE",
        "PIPE_PIPE",
        "PIPE_PIPE_SLASH",
        "PIPE_SLASH",
        "PLUS",
        "QMARK",
        "QMARK_AMP",
        "QMARK_HASH",
        "QMARK_HYPHEN",
        "QMARK_PIPE",
        "SLASH",
        "TIL",
        "TIL_EQ",
        "TIL_GTE_TIL",
        "TIL_GT_TIL",
        "TIL_LTE_TIL",
        "TIL_LT_TIL",
        "TIL_STAR",
        "TIL_TIL",
        "SEMI",
        "ErrorCharacter",
    ]

    grammarFileName = "BaserowFormulaLexer.g4"

    def __init__(self, input=None, output: TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.8")
        self._interp = LexerATNSimulator(
            self, self.atn, self.decisionsToDFA, PredictionContextCache()
        )
        self._actions = None
        self._predicates = None
